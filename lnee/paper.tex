\documentclass[graybox]{svmult}
%\documentclass[10pt,conference]{IEEEtran}
%\usepackage{textcomp}
%\usepackage{cite}
%\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{caption}
\usepackage{mathptmx}       % selects Times Roman as basic font
\usepackage{helvet}         % selects Helvetica as sans-serif font
\usepackage{courier}        % selects Courier as typewriter font
\usepackage{type1cm}        % activate if the above 3 fonts are
                            % not available on your system
\usepackage{xcolor}
\usepackage{url}
\usepackage{soul,color}
\soulregister\cite7
\soulregister\ref7
\soulregister\pageref7
\usepackage{makeidx}         % allows index generation
\usepackage{graphicx}        % standard LaTeX graphics tool
                             % when including figure files
\usepackage{multicol}        % used for the two-column index
\usepackage[bottom]{footmisc}% places footnotes at page bottom
%\usepackage{breqn}
% see the list of further useful packages
% in the Reference Guide

\makeindex             % used for the subject index
                       % please use the style svind.ist with
                       % your makeindex program

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\title*{An ICN-based Authentication Protocol for a Simplified LTE Architecture}
% Use \titlerunning{Short Title} for an abbreviated version of
% your contribution title if the original one is too long
\author{Alberto Compagno, Mauro Conti and Muhammad Hassan}
% Use \authorrunning{Short Title} for an abbreviated version of
% your contribution title if the original one is too long
\institute{Alberto Compagno \at Cisco Systems, Paris, France, \email{acompagn@cisco.com}
\and Mauro Conti \at University of Padova, Padova,  Italy, \email{conti@math.unipd.it}
\and Muhammad Hassan \at University of Padova, Padova,  Italy, \email{hassan@math.unipd.it}}
%
% Use the package "url.sty" to avoid
% problems with special characters
% used in your e-mail or web address
%
%%%%%%%%%%%%%


\maketitle

\abstract{Nowadays, the most diffused approach for supporting device mobility is
to implement specific mechanisms at link-layer (e.g., tunneling) supported by a
dedicated architecture (e.g., LTE architecture). While this approach can handle
mobility well within a singular network, it fails to provide a seamless Internet
connectivity when mobility occurs among different networks. To achieve
inter-networks mobility, researchers proposed to implement mobility management
protocols at the network layer. However, the current IP network layer has not
been designed for handling mobility, with the result that none of the proposed
IP-based methodologies is able to provide a satisfactory solution. Information
Centric Networking (ICN) is an emerging networking paradigm that provides a
better support for mobility than IP, enabling full mobility management at
network layer.\newline\indent
In this paper, we take a fresh look on mobility management and propose a
simplified LTE infrastructure that exploits the mobility support provided at the
ICN network layer. We revise the current device authentication protocol for LTE,
and we present a novel handover protocol that exploits the ICN communication
style. Compared to the protocol adopted in the current LTE, our proposals are
able to reduce the number of messages required to authenticate or
re-authenticate a device during mobility.}

\section{Introduction}
%
One obvious failure of IP is mobility management. Initially designed for the
existing static wired network technology, IP failed to chase the
technology evolution which allows nowadays wireless connectivity and mobile
devices. Different research efforts~\cite{perkins1997mobile,
  valko1999cellular, das2000telemip} tried to overcome the lack of mobility in
the IP design, however none of them was really able to provide a cost-effective
mobility mechanism. For this reason,  mobility management is nowadays provided at
link-layer, enabled only for specific wireless technologies (e.g.,
LTE, Wi-Fi) and confined in singular networks.

Information Centric Networking (ICN) is an emerging networking paradigm with a
natural support for mobility at network layer. In particular, in ICN the
host-centric communication (IP) approach is replaced with a content-centric
approach. Communication in ICN is triggered by consumer entities, who express
interest for specific content. The network will then deliver the consumer’s
interest to the producer entitled for generating the corresponding content, as
well as forward the content back to the consumers.

Two important ICN design choices provide to ICN a natural support for consumer
mobility. First, content is addressed by location-independent human readable
names, namely they do not express any reference to source or the destination of
packets (both interest and content). Second, neither consumers nor producers
require a network address (e.g., the IP address) to communicate. Only the name
of content is used to forward consumer's interests towards the corresponding
content, and the content back to the requesting consumer. This allows consumers
to forward interests as soon as an interface is available, as opposed to IP in
which a host is forced to wait for a mapping between the interface address and
its layer-3 address. Such content-based, location-independent communication
style has been shown to improve device mobility support with respect to the
current IP~\cite{ravindran2012supporting}, thus raising ICN as a possible future
solution to manage mobility at network layer.

In this paper, we propose a simplified LTE infrastructure that exploits the ICN
architecture to manage device mobility. Inspired by recent proposals that manage
mobility at ICN network
layer~\cite{auge2015anchor,zhang2014kite}, we propose a simpler
LTE architecture that does not require the Mobility Management Entity (MME);
i.e., the entity that guarantees an uninterrupted device connection during
mobility events. We use the ICN communication style to design a revised device
authentication protocol and a novel handover authentication protocol that reduce
the number of exchanged messages between the authenticating entities. In
particular, our handover authentication protocol exploits the ICN
synchronization protocol~\cite{zhu} to move the device security context (i.e.,
cryptographic material established during the mobile device authentication)
during the handover mechanism from the old to the new base station. Our
analytical evaluation shows that our authentication and handover authentication
protocols can reduce the device authentication delay when compared with the
current LTE authentication protocols.

\paragraph{Organization} In Section~\ref{sec:icn} we briefly introduce ICN. In
Section~\ref{sec:lte} we review the LTE authentication protocol and the handover
mechanism. Then, in Section~\ref{sec:system} we present our simplified LTE
infrastructure onto which we review the authentication protocol, proposed in
Section~\ref{sec:icn_eap}, and the handover mechanism, detailed in
Section~\ref{sec:icn_handover}. In Section~\ref{sec:evaluation} we evaluate our
proposed authentication protocols comparing to LTE authentication protocols with
respect to authentication delay. In Section~\ref{sec:security} we provide a
security discussion of our protocol and we propose the adoption of the
physical-security authentication to counter replay attacks. Finally, we conclude in
Section~\ref{sec:conclusion}.

\section{ICN overview}\label{sec:icn}
%
Communication in ICN is achieved via content distribution. ICN
directly names content, making it addressable and routable at network layer.
NDN~\cite{Zhang} and CCNx~\cite{Jacobson} are considered by the research
community to be two reference projects implementing the ICN paradigm.

The ICN communication model can be characterized as using a {\em pull model}:
content is delivered to {\em consumers} only upon (prior) explicit requests for
that content, i.e., each content delivery is triggered by a request for that
content. Content is generated by {\em producers} which are also responsible for
announcing its availability to the network. Consumers request desired content by
name, via {\em interest} packets. Names consist of one or more components with
a hierarchical structure, e.g., \texttt{/icn/cnn/politics}.

After receiving an interest, an ICN router forwards the interest towards the
content producer responsible for the requested name, using longest name-prefix
matching for routing information. Then, after the interest is delivered to the
content producer, the producer responds by sending the content into the network,
thus satisfying the interest. The requested content packet is then forwarded
towards the consumer, traversing -- in reverse -- the path of the preceding
interest.

Security in ICN follows a data-centric model. Each content is signed by the
producer, allowing consumers to verify integrity and data-origin authentication.

\section{Authentication and Mobile Management in LTE}\label{sec:lte}
%
The 3GPP consortium~\cite{3gppcons} defines Long Term Evolution (LTE) and System
Architecture Evolution (SAE) to be composed of two main architectural
components: the Evolved Terrestial Radio Access Network (E-UTRAN) and the
Evolved Packet Core (EPC) network \cite{Purkhiabani}. The LTE architecture is
depicted in Figure~\ref{fig:LTE}.

\begin{figure}[ht]
\centering
\includegraphics[scale = .5]{Images/ltearchipdfs3}
\caption{ LTE Architecture {\cite{alezabi}}}
\label{fig:LTE}
\end{figure}

The E-UTRAN is composed of a number of enhanced node base station, called
eNodeB, that provide wireless connectivity to the mobile devices (henceforth
called user equipment -- UE). The EPC contains different entities used
to manage mobility and that compose the SAE. Two notable SAE entities that are
of interest for this work are:

\begin{itemize}
\item \textbf{The Mobile Management Entity (MME).} MME plays a central role for
  management in LTE/SAE architecture. It contributes mainly in security,
  authentication, ID allocation of mobile devices (henceforth user equipment –- 
  UE) and roaming control in mobility scenarios. 
\item \textbf{The Home Subscriber Server (HSS).} HSS serves as home environment
  for the whole SAE/LTE architecture containing all the credentials of devices
  regarding authentication, security, identity and Quality of service (QoS).
\end{itemize}
  
Despite these above entities, the core network also have a Serving
Gateway (SGW) and a Packet Data Network Gateway (PGW). The role of SGW is to
serve the UE by sending and receiving packet data coming from and going to
eNodeB, acting also as a limited anchor of mobility service for UE. While PGW
connects the core network with other Packet Data Networks such as Internet
\cite{Cao}.

In the following, we describe the authentication protocol currently adopted by the
LTE infrastructure, the EAP-AKA protocol, and the handover protocol used to manage
mobility of the nodes.

\subsection{Authentication protocol}
%
The authentication protocol adopted in LTE networks is a four-party protocol
based on a pre-shared secret key that provides: (i) mutual authentication
between UE and the Network, (ii) distributes the necessary cryptographic
material to enable ciphering and integrity protection between the UE and the
MME, as well as the UE and the eNodeB. The entities involved in the EAP-AKA
protocol are:
\begin{enumerate}
  \item The UE that authenticates to the network.
  \item The eNodeB towards which the UE is connecting to the network.
  \item The MME which plays the role of an Authentication Center (AuC) that
    authenticates the UE.
  \item The HSS that stores the pre-shared key $k$ with the UE.
\end{enumerate}

The protocol provides mutual authentication between the UE and the network by
running the EAP-AKA protocol between the UE and the MME. The HSS will act as an
Authorization, Authentication and Accounting (AAA) server, providing to the MME
the needed information to perform the EAP-AKA protocol with the
UE. Figure~\ref{fig:eap-aka} shows the full authentication phase in the LTE/SAE
infrastructure.

The process of mutual authentication starts when UE enters in the radio range of
an eNodeB and issues an attach request to the MME. After receiving such request,
the MME requests to UE the International Mobile Subscriber Identity
($IMSI$)\footnote{$IMSI$ uniquely identifies a user in a cellular network. $IMSI$ is
  stored in the Subscriber Identity Module (SIM).}. Then, the MME requests to
the HSS the proper authentication vector (AV) to continue the authentication
protocol (i.e., perform the UE authentication and derive further keys to secure
the communication with the device). The AV is made of: an token $AUTN$, a random
number $RAND$, an expected authentication result $RES$ and a symmetric key
$K_{ASME}$. The triplet $AUTN$, $RAND$ and $XRES$ will be used to mutually
authenticate the UE and the network. The key $K_{ASME}$ will be later used by UE
and MME to derive further ciphering and integrity keys.

\begin{figure}[ht]
\centering
\includegraphics[scale = .5]{Images/lteakapdf4} 
\caption{EAP-AKA Authentication protocol \cite{alezabi}}
\label{fig:eap-aka}
\end{figure}

Once the MME receives the AV, it issues a user authentication message to the UE
passing the value $AUTN$ and $RAND$. Then, the UE derive its own $AUTN$
from the two value $k$ and $RAND$, and it compare its value of $AUTN$ with
the value received from the MME. The matches of these two values
authenticates the network. The protocol concludes with UE deriving the $XRES$ and
forwarding it back to the MME which matches it against $RES$. In this case,
if the two values matches, the UE is considered to be authentic. Along with
$XRES$, the UE derives $K_{ASME}$ too.

Once the mutual authentication is completed, both UE and MME can derive the
needed keys to enable ciphering and integrity protection for the communication
between the UE and the MME (i.e., Non access stratum security). Moreover, the UE
and the eNodeB will both derive ciphering and integrity to protect the message
delivery between them (i.e., Access stratum security)~\cite{Køien}. To perform
this last step, the MME will share a key $K_{eNodeB}$ with the eNodeB, which
will be calculated in the UE too. Such key will be then used to derive the
integrity and ciphering keys between UE and eNodeB.

\subsection{Handover protocol}
%
LTE implements two different handover schemes. The first is a centralized
approach in which the MME acts as a connection point receiving the handover
requests from the source eNodeB (i.e., the eNodeB the UE is going to leave) and
forwarding it to the target eNodeB (i.e., the eNodeB the UE is going to connect
to). The second is a distributed approach in which the source eNodeB directly
communicate with the target eNodeB exploiting a direct link between them called
X2 link.

Each of the two handover approaches goes with its own $K_{eNodeB}$ derivation
mechanisms, i.e., every time a UE moves to a different eNodeB, a new key
$K_{eNodeB}$ is generated to prevent previous eNodeB (honest or controlled) to
decrypt or modify the packet exchanged between the new eNodeB and UE. In the
centralized approach, the new $K_{eNodeB}$ is sent from the MME to the target
eNodeB, while in the distributed approach the source eNodeB generates and send
the new $K_{eNodeB}$ to the target eNodeB. 

\section{Simplified LTE architecture for ICN}\label{sec:system}
%
In this section, we propose a revised LTE infrastructure in which both the
access network and the core network implement the ICN stack. Mobility is managed
at network layer in a distributed way as proposed in~\cite{auge2015anchor}. Such
approach does not require any central entity for managing mobility, such as the
MME. For this reason, in our revised LTE infrastructure the MME entity is no
longer part of the architecture. The only available entities are:

\begin{itemize}
\item \textbf{HSS}. Like in the original LTE architecture, HSS contains all the
  UE information regarding authentication, security, identity and Quality of
  service (QoS). In our revised LTE, the HSS is a producer that provides for the
  content it is storing. We assume that the HSS published its content under the
  namespace \textit{/UE/login}
\item \textbf{UE}. A UE represents the device that wants to connect to the
  cellular network.
\item \textbf{eNodeB}. The network will be formed of many eNodeBs, acting as point
  of access to the network for the UEs.
\item \textbf{ICN Core Router}. In our simplified LTE architecture, the core
  network is ICN routers.
\end{itemize}

We assume that all the eNodeBs and ICN routers have the necessary routing and
forwarding information to deliver interests to the HSS. Moreover, eNodeBs and
ICN routers trust the HSS as the producer for the UE credentials.

This can be achieve either by installing the public key of the HSS in each
eNodeB or by involving a root of trust who sign the public key of the HSS, i.e.,
it creates a certificate for the HSS. In the latter case, an eNodeB has only to
verify the HSS certificate once and we assume it to be at bootstrapping time.

\subsection{Authentication protocol in ICN}\label{sec:icn_eap}
%
We propose an UE authentication protocol in our revised LTE infrastructure that
exploits the ICN communication style. Similarly to the LTE authentication
protocol, our protocol adopts the EAP-AKA to provide:

\begin{itemize}
\item Mutual authentication between the UE and the cellular network.
\item Distribution of the cryptographic material to provide integrity and
  ciphering between UE and the eNodeB connected to the eNodeB.
\end{itemize}

Our proposal simplifies the original LTE
authentication protocol in at least two aspects: (i) it involves three entities
(i.e., UE, eNodeB and the HSS) rather than four, thus reducing the communication
delay, (ii) it performs the main part of the protocol between the UE and the
eNodeB in order to minimize the overall network overhead. While UE and MME are
usually multi-hops away one from the each other, UE and eNodeB are instead
separated only from one hop. Therefore, running the most of the protocol among
UE and eNodeB will reduce the number of messages that travels in the network
(i.e., from the eNodeB to the HSS). The work
in~\cite{Compagno:2016:OSP:2984356.2984374} has already shown the advantage of a
similar approach. Figure~\ref{fig:interest-s1} depicts our authentication 
protocol over ICN.

\begin{figure}[ht]
\centering
\includegraphics[scale = 0.5]{Images/interest-s3n}
\caption{EAP-AKA over ICN}
\label{fig:interest-s1}
\end{figure}
%
The protocol starts with UE issuing an interest requesting to access the
network (Step 1). The last component of the interest contains the UE's identity;
the $IMSI$. Once eNodeB knows the UE's $IMSI$, it issues an interest to retrieve the
AV from the HSS (Step 2). In our proposal, AV is made of: $AUTN$, $RAND$, $XRES$
and a key for access eNodeB $K_{ANB}$. While the first three are the same
parameters used in the original LTE authentication protocol, $K_{ANB}$ is
specific for our proposal. Its purpose is allows UE and eNodeB to derive
integrity and ciphering key, even in case of handover.

The protocol then continues with the HSS that satisfies the interest issued in
Step 2 with a content carrying the AV (Step 3), thus allowing the eNodeB to
satisfy the first interest issued by UE with a content carrying $AUTN$, 
$RAND$ (Step 4). Like in the original LTE authentication protocol, UE 
calculates its own version of $AUTN$ and match it over the $AUTN$ received from
the eNodeB. This check authenticates the network. The protocol then concludes
with the eNodeB requesting the authentication from UE (Step 5), which will reply
with a content transporting $XRES$ in its payload (Step 6).

It is worth mentioning that every content packet in ICN must be authenticated
with the producer's key (either symmetric or its private key). Therefore, in our
protocol we use the HSS's private key, $SK_{HSS}$ to sign each content generated
by the HSS, while we use $K_{ANB}$ to authenticate the content exchanged between
UE and eNodeB (steps 4 and 6 in Figure~\ref{fig:interest-s1}).

\subsection{Handover protocol in ICN}\label{sec:icn_handover}
%
The handover mechanism in our proposal is performed through an authentication
handover module (AHM). AHM is an application running on every eNodeB that
prepares relevant eNodeBs (i.e., stores and shares the crypto material to
authenticate UE) before UE arrives. In particular, AHM predicts the future
location of user \cite{zeng} and estimates the next area that UE will pass
through \cite{Hyeyeon,gprs,pollini,chan}. Once the area has been calculated, it
identifies the group of eNodeBs, namely relevant eNodeBs, covering such area and
shares with them the information to authenticate UE.

\subsubsection{Handover and UE re-authentication}\label{subsubsec:handover}
%
The authentication handover module predicts the set of relevant eNodeBs
extrapolating the movement of UE using physical attributes i.e. location,
velocity and direction. Moreover, AHM maintains a dataset related to each UE
containing its relevant authentication material. Once the relevant eNodeBs have
been identified, AHM exploits ChronoSync~\cite{zhu} to share with them the
dataset related to the UE. 

After the EAP-AKA protocol has been completed, the authenticated UE and eNodeB
share $K_{ANB}$. At this point, AHM starts predicting the set of relevant
eNodeBs and it stores and shares a new key $K_{ANB}*$ calculated as follows:
%
\begin{equation}
K_{ANB}* = KDF (K_{ANB},RAND).
\label{eq:kanb}
\end{equation}

When the UE moves to one of the relevant eNodeB, it derives $K_{ANB}*$ and it
authenticates to the new eNodeB by sending an interest carrying a new random
number $RAND*$ and a message authentication code (MAC) calculated from the
interest name (and $RAND*$, later used again) with $K_{ANB}*$. The eNodeB then
replies with a content authenticated with $K_{ANB}*$. If both interest and
content are authentic, then UE and the eNodeB are authenticated by each other
and they can further derive ciphering and integrity key for securing their
communication. At this point, AHM running in the eNodeB starts predicting the
set of relevant eNodeBs and it shares a new key $K_{ANB}*$ calculated as
described in Equation~(\ref{eq:kanb}) (in this case, the previous value of
$K_{ANB}*$ will replace $K_{ANB}$ and $RAND*$ will replace $RAND$ in the
equation).

\subsubsection{Synchronization of the key access eNodeB in the relevant eNodeBs}
%
\sloppy
Synchronization of $K_{ANB}*$ is performed through the ChronoSync
protocol. ChronoSync synchronizes the state of a given dataset among multiple
ICN entities~\cite{zhu}. The protocol works on the idea to encode the state of
the dataset of each entity into crypto digest form (i.e., SHA256) called
$state digest$, or digest in short. These state digests are then exchanged among
all the entities participating in particular synchronization group. Each entity
depending upon the state of its own dataset calculates the state digest, and
sends a broadcast interest to all the other entities in that group, containing
that state digest. On receiving such interest, if the value of the incoming
digest is identical comparing to the value maintained locally, no action will be
taken and called as stable state. Otherwise, the difference of the dataset state
is directly inferred and sent in response to the sync interest~\cite{zhu}. With
the knowledge of the up-to-date state dataset, an ICN entity (or one of its
running application) can then decide to fetch the new content in the dataset.

In our proposal, ChronoSync synchronizes the state of UE's dataset, i.e., the
key $K_{ANB}*$, on each relevant eNodeB predicted by the AHM. Therefore, the
AHM's dataset running on each relevant eNodeB will be notified of the new key
$K_{ANB}*$ and will fetch it from the eNodeB sending the notification. After the
new $K_{ANB}*$ is fetched, a relevant eNodeB is ready to authenticate UE and to
perform the handover as explained in
Section~\ref{subsubsec:handover}. Figure~\ref{fig:ChronoAuth} shows the
communication between relevant eNodeBs during the synchronization process.
%
\begin{figure}[ht]
\centering
\includegraphics[scale = .8 ]{Images/intereNodeBchrono6}
\caption{Synchronization}
\label{fig:ChronoAuth}
\end{figure}
%
The synchronization of the dataset state and the fetching of $K_{ANB}*$ require
the definition of two namespaces, namely the \texttt{sync data namespace} and
the \texttt{application data namespace}. The \texttt{sync data namespace} is
used to carry interests and contents used to synchronize the dataset state using
ChronoSync. The purpose of \texttt{application data namespace} is to have
routable name prefixes, so that interests can be forwarded towards the relevant
eNodeBs directly, as AHM behaves like a producer in each
eNodeB. Figure~\ref{subfig:app_namespace} and \ref{fig:sync_name} show an
example of content name in the \texttt{sync data namespace} and in the
\texttt{application data namespace} respectively.
%
\begin{figure}[h]
\centering
 \includegraphics[scale = 0.60]{Images/chronofig1final1}  
\caption{\small{Application data name}}
  \label{subfig:app_namespace}
\end{figure}
\begin{figure} [h]

  \centering 
 \includegraphics[scale = 0.60]{Images/chronofig2final1} 
 \caption{\small{Sync data name}}
  \label{fig:sync_name}
\end{figure}
%
Figure~\ref{subfig:app_namespace} shows a content in the application data
namespace. The first part of the application data name (indicated
as 1) represents the routable prefix for particular eNodeB with its unique ID.
Part (2) represents the name of a particular application to synchronized. It shows
the name of the process which is responsible for handling that particular
interests. The data generated by eNodeB is named sequentially, for example with
the initial value of $K_{ANB}*$ computed by AHM has a sequence number zero. Whenever a
new value of $K_{ANB}*$ is generated, this sequence number is incremented by one. So,
the last part (3) is the sequence number of the latest $K_{ANB}*$. 

Sync data namespace, depicted in Figure~\ref{fig:sync_name}, also consists of
three parts. Part (1) is the prefix ensuring the broadcast namespace for the
given domain created by AHM ascending index.
In particular, such prefix will be shared among all the eNodeBs along
propagating path of the user (index i.e., eNodeB1, eNodeB2, eNodeB3, ...). This will
allow a synchronization interest to reach all the relevant eNodeB. In the Part
(2) similarly as application data names 
defines the name of application, which shows that particular interest are
responsible for authentication request of specific user. The last part notifies
the recent state digest of the interest sender, i.e., the digest of its current
$K_{ANB}*$.
   
\section{Evaluation}\label{sec:evaluation}
%
In this section we compare the performance of UE authentication in our proposed
LTE infrastructure for ICN with today's LTE infrastructure. The performance
comparison evaluates the delay occurred during authentication 
of a UE, and also re-authentication during handover comparing to handover
authentication in LTE, particularly in the distributed handover.

\subsection{Authentication delay evaluation}
%
In order to evaluate the authentication delay required in the two infrastructure
we define the time taken by the method to complete the authentication process as the
total authentication delay ($D_{auth}$). $D_{auth}$ can be further divided
 into three components: the delay of the EAP messages transmission
($D_{trans}$), the EAP messages treatment delay ($D_{tre}$) considering data base
access, key and tag generation, computation, encryption/decryption, and the
propagation delay ($D_{prop}$)~\cite{Idrissi}.

$D_{tre}$ is the delay occurred during EAP messages treatment/processing on
each node, which depends on LTE servers and UE performance (e.g., CPU,
memory). We assume that our proposed protocol and standard EAP-AKA use same key 
encryption with similar key sizes. Therefore we can say that treatment delay is
identical in both protocols, and also considering performance of LTE servers, we
assume transmission delay is insignificant. Thus total authentication delay
depends upon the propagation delay $D_{prop}$~\cite{Idrissi}.

$D_{prop}$ can be divided in four sets:
$D_{prop(UE-eNodeB)} $ propagation delay between UE and eNodeB,
$D_{prop(eNodeB-MME)}$ propagation delay between eNodeB and MME,
and $D_{prop(MME-HSS)}$ propagation delay between MME and HSS.  The total
authentication delay for EAP-AKA in the current LTE then can be expressed
as~\cite{Shidani,Idrissi}:
\begin{equation}
\begin{split}
D_{auth(EAP-AKA)}=D_{tre(EAP-AKA)} + 5D_{prop(UE-eNodeB)}   + 5D_{prop(eNodeB-MME)} \\
+ 2D_{prop(MME-HSS).}
\end{split} 
\label{eqn:eap-lte}
\end{equation}
%
From Figure~\ref{fig:eap-aka} and Equation~(\ref{eqn:eap-lte}) we
calculated that total number of messages exchanged between entities i.e. UE, eNodeB
and MME (which are 5, 5 and 2 respectively) multiplies the propagation delay
between them. Also, the total authentication delay of EAP-AKA in our ICN based
architecture can be expressed as, Figure~\ref{fig:interest-s1}:
%
\begin{equation}
D_{auth-ICN(EAP-AKA)}=D_{tre(EAP-AKA)}+
4D_{prop(UE-eNodeB)}+
2D_{prop(eNodeB-HSS).}
\label{eqn:eap-icn}
\end{equation}
%
From equations~(\ref{eqn:eap-lte}) and~(\ref{eqn:eap-icn}), we note that our
approach reduces the propagation delay between UE and eNodeB by one. This is
because EAP-AKA over ICN requires one message less that the EAP-AKA protocol
adopted in the current LTE. 

Another improvement in the authentication delay is due the removal of the MME
in our approach. This, in turn, reduces the number of messages exchanged with the
UE and entities in the EPC. In particular, our approach exchanged only two
messages between the UE and the HSS, with a propagation delay that is
indicated as $2D_{prop(eNodeB-HSS)}$ in the Equation (3). Instead, the current LTE
requires 5 messages exchanged from the UE and the MME, and 2 messages
exchanged from MME and HSS. The propagation of those messages is indicated
in Equation (2) as $5D_{prop(eNodeB-MME)}$ and $2D_{prop (MME-HSS)}$,
respectively.
 
\textcolor{red}{Table~\ref{table1} compares the authentication delay of the
EAP-AKA in LTE and our EAP-AKA over ICN. We assume that
$D_{prop(eNodeB-HSS)} \cong D_{prop(eNodeB-MME)} + D_{prop (MME-HSS)}$ which we
consider to be a pessimistic assumption for our approach. We expect that
an architecture without MME will not increase the propagation delay between
the UE and the HSS, but in the best case it will reduce it. However, since we
cannot evaluate such improvement, we compare the authentication delay under
the worst case scenario for our mechanism.}
%
\begin{table}
\centering
\captionsetup{justification=centering}
\caption{Authentication delay comparison}
\begin{tabular}{ |c|c|c|c } 
\hline
{Authentication Protocol} & {Authentication delay ms} \\
\hline
EAP-AKA & 1244 \\
 \hline
ICN(EAP-AKA) & 1019  \\
\hline
\end{tabular}
\label{table1}
\end{table}

\textcolor{red}{To evaluate the authentication delay we used the experimental
values found in~\cite{Idrissi} and~\cite{shidhani2}. In~\cite{Idrissi} the
average value for complete EAP-AKA authentication delay is 1244 ms. In
\cite{shidhani2} authors approximated the propagation delay between eNodeB to
MME is 75 ms. We used such value to calculate the average authentication delay
in our protocol, which results 1019 ms.}

\subsection{Handover authentication delay}
%
While calculating re-authentication delay using same Equation~(\ref{eqn:eap-icn}),
we found that it depends on the propagation delay between source to target
eNodeBs. The handover scenario we have assumed during evaluation for LTE is the
X2-based handover. In X2 handover, authentication material derived during full
EAP-AKA is transferred by source eNodeB directly to target eNodeB exploiting the
direct X2 link. This scenario, also named as horizontal handover is fair to
compare with our proposed protocol, as we also proposed a network infrastructure
without the MME. Therefore for LTE re-authentication delay during inter
eNodeB/X2 handover can be calculated as the number of messages exchanged from
source to target eNodeBs.
%
\begin{equation}
D_{hand-auth(EAP-AKA)}=D_{prop(Src_{eNodeB}-Trg_{eNodeB}).}
\label{eqn:lte-hand}
\end{equation}
%
From the work in~\cite{masud2015survey}, we calculate the total number of messages
between eNodeBs and therefore:
%
\begin{equation}
D_{hand-auth(EAP-AKA)}= 5D_{prop(Src_{eNodeB}-Trg_{eNodeB}).}
\label{eqn:lte-hand_ebs}
\end{equation}
%
Also using same equation for calculating handover authentication in our proposed
protocol Figure~\ref{fig:ChronoAuth}, we found: 
%
\begin{equation}
D_{hand-auth-ICN(EAP-AKA)}= 3D_{prop(Src_{eNodeB}-Trg_{eNodeB}).}
\label{eqn:icn-hand_ebs}
\end{equation}

\textcolor{red}{From equations~(\ref{eqn:lte-hand_ebs})
and~(\ref{eqn:icn-hand_ebs}), we evaluated that our re-authentication protocol is
requires two less messages propagated between eNodeBs than the handover in
LTE. We expect that decreasing the number of messages will produce a lower
handover authentication delay. To confirm it, we estimate the two handover
authentication delays and we report our comparison in Table~\ref{table2}.}
%
\begin{table}
\centering
\captionsetup{justification=centering}
\caption{Handover Authentication delay comparison}
\begin{tabular}{ |c|c|c|c } 
\hline
{Handover Authentication Protocol} & {Authentication delay ms} \\
\hline
EAP-AKA & 34.30 \\
 \hline
ICN(EAP-AKA) & 20.58  \\
\hline
\end{tabular}
\label{table2}
\end{table}

\textcolor{red}{To evaluate the two handover authentication delay, we estimated the
propagation delay from source eNodeB to target eNodeB on X2
link. From~\cite{Xiao}, we calculated that the propagation delay is
approximately 6.86 ms. Using equations~(\ref{eqn:lte-hand_ebs})
and~(\ref{eqn:icn-hand_ebs}) we evaluate that our handover authentication
protocol reduces the authentication delay by the value of approximately 13.72
ms}.



\section{Security discussion}\label{sec:security}
%
\textcolor{red}{In this section we provide a security discussion about the two
protocols that we propose: the authentication protocol and the handover protocol.}

\subsection{Authentication protocol}
%
\textcolor{red}{We argue that the security of our authentication protocol is
comparable to the security provided by the authentication protocol in LTE. Both
the authentication protocols exploit the standard EAP-AKA protocol
without changing any of the steps described in the EAP-AKA
specification. Therefore, all the security considerations made for EAP-AKA are
still valid for our EAP-AKA over ICN~\cite{arkko2009improved}.}

\subsection{Handover protocol}
%
\textcolor{red}{Our handover protocol is resilient to an external adversary
(i.e., an adversary that does not own a valid $IMSI$ for the HSS) with the
goal of authenticating itself to an eNodeB. In order to achieve it goal, the
adversary must be able to issue an interest with a valid $MAC$ to the
eNodeB. However, to be able to successfully generate the $MAC$ for the interest
the adversary must know, or obtain, a valid $K_{ANB}*$.}

\textcolor{red}{Our handover authentication protocol make it unfeasible for an external
adversary to obtain a valid $K_{ANB}*$. This due to the fact that $K_{ANB}*$ is 
never transmitted between UE and the target eNodeB, but rather calculated from
$K_{ANB}$ and $RAND$. Only $RAND$ is transmitted between a UE and the target
eNodeB, therefore as long as the key derivation function requires only $K_{ANB}$ to
be secret, the adversary has no way to derive $K_{ANB}*$. Unfortunately, the
adversary might eavesdrop and replay $RAND$ to another eNodeB and authenticate
to it as a genuine UE. While this attach will not let the adversary to
communicate through the network (it will not be able to generate the correct
integrity and ciphering keys, which are derived from $K_{ANB}$), it can
temporarily waste some state in the eNodeB. In fact, every time an eNodeB
authenticate and UE (genuine or not), it must reserve the necessary state to
handle a communication with it (e.g., the integrity and ciphering key). To
protect the network from such replay attack, we propose to adopt a physical-layer
authentication.}

\textcolor{red}{Physical-layer authentication uses the subtle features of
the physical-layer signal to provide a secure device authentication between two
trusted nodes in the presence of an adversary/eavesdropper with unlimited
computational power~\cite{Yener,yangming}. We exploit the RF finger printing
technique~\cite{Ureten} to provide physical layer authentication in our
handover authentication protocol. The RF finger print mainly depends on the
differences of each trusted transmitter components, power supplies and
environmental factors, which are extracted from RF signal. From~\cite{yangming}
different equipment/nodes (UEs) can be identified by measuring the specific value
extracted from their unique RF fingerprints denoted as $ |A(t)|$. In our
handover authentication protocol we use $|A(t)|$ as an entity replacing $RAND$
to provide physical-layer authentication. So in Equation~(\ref{eq:RF}), $K_{ANB}*$
is the key derivation function of key for source eNodeB and RF finger print of
the UE authenticated initially to mitigate the presence of
eavesdropper. However, the functioning of AHM for predicting the relevant
eNodeBs and ChronoSync protocol to synchronize the dataset among multiple ICN
entities will follow the same as described above.}
\begin{equation}
K_{ANB}* = KDF (K_{ANB}, |A(t)|).
\label{eq:RF}
\end{equation}

\section{Conclusion}\label{sec:conclusion}
%
In this work we propose a revised LTE infrastructure that exploits the ICN
communication paradigm to manage UE authentication and transporting the UE
security context from the old eNodeB to the new one. We design a new handover
mechanisms that does not require any central entity, e.g., the MME, to
distribute the cryptographic material to the new eNodeB.

Our approach reduces the complexity of the LTE infrastructure thus making it
simpler, easier to manage and more cost-effective for network providers. We
believe that this is a valid reason that would lead network provides for
deploying ICN in their cellular infrastructure.

\bibliographystyle{unsrt}
\bibliography{sigproc}
\end{document}
